include:
  - local: ci-includes/alpine-image.yml
  - local: ci-includes/subdir/jobs.yml

variables:
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2

stages:
- build
- test
- last

.fail-job:
  stage: build
  image: busybox:latest
  script:
    - df -h
    - /bin/false

emulator-linux-ubuntu1804-install:
  image: ubuntu:18.04
  before_script:
    - apt-get update
    - apt-get install -y python3 python3-pip
  script:
    - cd emulator
    - pip3 install .
    - cd ..
    - gle -l
    - glp --help

emulator-linux-ubuntu2004-install:
  extends:
    - emulator-linux-ubuntu1804-install
  image: ubuntu:20.04

emulator-linux-ubuntu2204-install:
  extends:
    - emulator-linux-ubuntu1804-install
  image: ubuntu:22.04

emulator-linux-test:
  image:
    name: docker:stable-dind
    entrypoint: [""]
  stage: build
  coverage: '/Total coverage: \d+\.\d+%/'
  before_script:
    - nohup dockerd-entrypoint.sh 2>&1 &
  script:
    - apk add py3-pip py3-yaml git
    - python3 -m venv /tmp/venv
    - . /tmp/venv/bin/activate
    - ps -ef
    - sleep 5
    - docker info
    - pip install -r test_requirements.txt
    - cd emulator
    - pip install -e .
    - python -m pytest --cov gitlabemu --junitxml=test-results.xml -vv . --cov-report=xml:pytest-coverage.xml
    - python -m build
  artifacts:
    when: always
    reports:
      junit:
        - emulator/test-results.xml
      coverage_report:
        coverage_format: cobertura
        path: emulator/pytest-coverage.xml


emulator-apple-test:
  tags: [ shared-macos-amd64 ]
  image: macos-11-xcode-12
  stage: build
  script:
    - pip install -r test_requirements.txt
    - cd emulator
    - pip install -e .
    - python3 -m pytest  --junitxml=test-results.xml -vv . --cov-config .coveragerc.apple  --cov-report=xml:pytest-coverage.xml
  artifacts:
    reports:
      junit:
        - emulator/test-results.xml
      coverage_report:
        coverage_format: cobertura
        path: emulator/pytest-coverage.xml

emulator-linux-run:
  variables:
    DOCKER_HOST: tcp://docker:2375/
  services:
    - docker:dind
  stage: build
  timeout: 15m
  image: docker:stable-git
  before_script:
    - cd emulator
    - apk add python3
    - apk add py3-pip py3-yaml
    - python3 -m venv /tmp/venv
    - . /tmp/venv/bin/activate
    - pip install -e .
  script:
    - gle -c ../test-ci.yml --full linux-build-later
    - gle -c ../test-ci.yml linux-docker-job
    - HERE=$(pwd)
    - cd /tmp && gle -c ${HERE}/../test-ci.yml linux-shell-job

emulator-windows-run:
  stage: build
  when: manual
  allow_failure: true
  tags:
    - cunity-windows-2019-docker-shell
  script:
    - cd emulator
    - c:\python37\python.exe locallab.py -c ..\test-ci.yml windows-docker-job
    - c:\python37\python.exe locallab.py -c ..\test-ci.yml windows-shell-job

emulator-windows-test:
  stage: build
  tags:
    - shared-windows
    - windows
    - windows-1809
  script:
    - choco install -y python --version=3.8.0
    - c:\python38\python -m pip install -r test_requirements.txt
    - cd emulator
    - c:\python38\python -m pip install -e .
    - c:\python38\python -m pytest -vv . --cov gitlabemu --cov-config .coveragerc.windows

runner-windows-test:
  stage: build
  tags:
    - shared-windows
    - windows
    - windows-1809
  script:
    - choco install -y python --version=3.8.0
    - c:\python38\python -m pip install -r test_requirements.txt
    - cd emulator
    - c:\python38\python -m pip install -e .
    - cd ..
    - cd runner
    - c:\python38\python -m pip install -e .
    - c:\python38\python -m pytest -s -vv .

runner-python3-linux-test:
  image: python:3.7
  stage: build
  script:
    - python3 -m venv /tmp/venv
    - . /tmp/venv/bin/activate
    - pip install -r test_requirements.txt
    - pip install -e emulator
    - cd runner
    - pip install -e .
    - gitlab-runner.py --help
    - python3 -m pytest .
    - python3 -m build
  artifacts:
    reports:
      junit:
        - runner/test-results.xml
        - runner/dist/*.whl

runner-child-generator:
  stage: build
  image: python:3.7
  script:
    - cd runner
    - python generate_child.py
  artifacts:
    paths:
      - runner/generated/child-pipeline.yml
      - runner/generated/child-tag.txt

runner-parent-launcher:
  stage: test
  needs:
    - runner-child-generator
  trigger:
    include:
      artifact: runner/generated/child-pipeline.yml
      job: runner-child-generator
    strategy: depend

runner-child-runner:
  stage: test
  image: python:3.9
  timeout: 5m
  needs:
    - runner-child-generator
  before_script:
    - pip install -e emulator
  script:
    - cd runner
    - pip install -e .
    - mkdir runner
    - cd runner
    - gitlab-runner.py --register https://gitlab.com
      --type shell
      --tag $(cat ../generated/child-tag.txt)
      --regtoken="${PY_RUNNER_REG_TOKEN}"
      --desc dynamic-runner
    - touch http.trace
    - tail -f http.trace &
    - echo Running a job..
    - gitlab-runner.py
      --trace-http
      --start gitlab-runner.yml
      --once
    - kill %1
    - echo Job ended..
  after_script:
    - cd runner/runner && gitlab-runner.py --unregister
  artifacts:
    when: always
    paths:
      - runner/runner/*.trace

envs:
  stage: build
  image: alpine:3.14
  script:
    - env|grep ^CI_|sort

from-gitlab-list:
  stage: last
  image: python:3.7
  needs:
    - job: emulator-linux-test
      artifacts: false
  before_script:
    - python3 -m venv /tmp/venv
    - . /tmp/venv/bin/activate
    - pip install -e emulator
  script:
    - echo " == listing all jobs in current pipeline ${CI_PROJECT_PATH}/${CI_PIPELINE_ID} .. "
    - gle --list --from ${CI_SERVER_HOST}/${CI_PROJECT_PATH}/${CI_PIPELINE_ID}

from-gitlab-export:
  stage: last
  image: python:3.7
  needs:
    - job: emulator-linux-test
      artifacts: false
  before_script:
    - python3 -m venv /tmp/venv
    - . /tmp/venv/bin/activate
    - pip install -e emulator
  script:
    - echo " == listing complted jobs in current pipeline ${CI_PROJECT_PATH}/${CI_PIPELINE_ID} .. "
    - gle --list --completed --from ${CI_SERVER_HOST}/${CI_PROJECT_PATH}/${CI_PIPELINE_ID}
    - echo " == exporting  emulator-linux-test completed job .. "
    - gle --from ${CI_SERVER_HOST}/${CI_PROJECT_PATH}/${CI_PIPELINE_ID} --export archive  emulator-linux-test
    - find archive -type f

quick:
  image: alpine:3.14
  stage: build
  script:
    - date > date.txt
  artifacts:
    paths:
      - date.txt

needs-no-artifacts:
  image: alpine:3.14
  stage: test
  needs:
    - job: quick
      artifacts: false
  script:
    - mkdir date.txt # will fail if date.txt was fetched from the quick job

needs-artifacts:
  image: alpine:3.14
  stage: test
  needs: [ quick ]
  script:
    - ls -l date.txt

.echo:
  stage: test
  script:
    - echo hello


